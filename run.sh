#!/bin/bash
apt-get update -y
apt-get install aria2 -y
pip install -r /content/preprocessing/requirements.txt --extra-index-url https://download.pytorch.org/whl/cu118 -U
apt-get install ffmpeg libsm6 libxext6 -y
mkdir checkpoints
aria2c --console-log-level=error -c -x 16 -s 16 -k 1M https://huggingface.co/phoenix-1708/DR_NED/resolve/main/checkpoints.zip -d /content/checkpoints -o checkpoints.zip
unzip /content/checkpoints/checkpoints.zip -d /content/checkpoints
rm -rf /content/checkpoints/checkpoints.zip
