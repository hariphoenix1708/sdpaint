import base64
import httpx


async def inpaint(image: bytes, mask: bytes) -> bytes:
    image = base64.b64encode(image).decode()
    mask = base64.b64encode(mask).decode()

    async with httpx.AsyncClient(timeout=None) as client:
        r = await client.post(
            "https://comic-caribou-frankly.ngrok-free.app/sdapi/v1/img2img",
            json={
                "init_images": [image],
                "resize_mode": "1",
                "denoising_strength": 1.0,
                "mask": mask,
                "mask_blur": 10,
                "inpaint_full_res": False,
                "inpaint_full_res_padding": 32,
                "inpainting_mask_invert": 0,
                "initial_noise_multiplier": 1,
                "prompt": "sexy girl, sexy body, adaptive body size, thick and healthy skin, nude, naked, adaptive sexy pose, dusky nipples, adaptive natural skin tone, (evenly toned skin), even skin texture, natural skin texture, natural folds, gorgeous, shaved pussy, beautiful, thick thighs, nsfw, raytracing, shading and lightning, 8K post-processing, UHD, HDR10, adaptive sharp focus, tack sharp, film grain, crystal clear, photorealism, RAW",
                "negative_prompt": "child, teen, out of frame, inpainting trace, cartoon, 3d render, bad render, airbrushed, illustration, oil painting, painting, photoshop, photoshopped, 3rd party edit, drawing, blur, sketches, muscles, CGI, 3d model, doll, imperfect skin, plastic skin, skin glow, skin glitter, skin reflection, imperfect skin tone, imperfect skin texture, blurry, bad anatomy, glitchy, saturated colors, oversaturation, cartoon, low-res, text, watermark, error, cropped, worst quality, low quality, jpeg, artifacts, strange colors, boring, lackluster, High pass filter, signature, deformed nipples, imperfect nipples, oversized boobs, ugly, deformed, mutation, mutated, disfigured, gross proportions, long neck, missing arms, extra arms, poorly drawn hands, imperfect hands, mutated hands, extra hands, bad hands, badhandv4, bad hands via net, distorted fingers, deformed fingers, extra fingers, fused fingers, missing fingers, skinny, fit, fat, muscular, muscular body, plastic body, (deformed body), out of focus body, extra legs, missing legs, deformed legs, extra limbs,malformed limbs, extra feet, missing feet, deformed feet, mutated feet, disfigured, duplicate, multilated, By bad artist, bad art, bad proportions, anaestheticXL_AYv1, flat",
                "steps": 30,
                "sampler_index": "DPM++ 2M Karras",
            }
        )
    assert r.is_success

    result_image = r.json()["images"][0]
    return base64.b64decode(result_image)

